<?php

if (is_file(__DIR__ . '/vendor/autoload.php')) {
    require_once __DIR__ . '/vendor/autoload.php';
} elseif(is_file(__DIR__ . '/../../autoload.php')) {
    require_once __DIR__ . '/../../autoload.php';
}

use Symfony\Component\Console\Application;

$application = new Application();

$commands = [
    new \KayStrobach\Releasy\Command\ReleaseCommand(),
    new \KayStrobach\Releasy\Command\ExtensionListCommand(),
    new \KayStrobach\Releasy\Command\PackageCommand(),
    new \KayStrobach\Releasy\Command\Git\CheckCommand(),
    new \KayStrobach\Releasy\Command\Git\GitDumpChangelogCommand(),
    new \KayStrobach\Releasy\Command\TYPO3\SetExtensionVersionCommand(),
    new \KayStrobach\Releasy\Command\TYPO3\SetDependenciesInDistributionCommand(),
    new \KayStrobach\Releasy\Command\Git\GitCreateTagCommand(),
];

foreach ($commands as $command)
{
    $application->add($command);
}

$application->run();
