# Version 

* f73e8d60 [TASK] move typo3 dependencies to drk_template
* ef586e35 [TASK] raise dependency to core for app_buttons
* 42d73065 [TASK] move distribution2016
* 703c9d73 [BUGFIX] use distribution instead of template as dependency
* 64111082 [TASK] cleanup distribution angebote
* 93da39aa [TASK] Updates for composer
* db0352ca [TASK] ddev modfications
* 2b86a583 [TASK] raise docs version
* ca7f2f6d [DOC] fix warnings in testplan
* c0e41296 [TASK] add php dependencies
* f58c2be0 [DOC]  Update docs
* b8ddbc34 [TASK] Update DDEV Docs renderer
* 7db2ec71 [TASK] Update Docs
* 7c067597 [TASK] drk_courseregistration: improves error reporting
* fe0a4703 [TASK] CourseregistrationForm: Wording
* 0a2273d8 [TASK] drk-supply-finder - fix seo_text error if not available
* 4bad3043 [TASK] Bugfix flexform viewhelper
* d377c27f Merge branch 'master' of gitlab.com:drk-intern/typo3-main-distribution
* 2b3c27e7 [TASK] use Dev context on platform.sh
* ada03684 [TASK] react for main page in config.yaml
* 6e3e65a9 [TASK] Update platform config
* 6ec163d5 [FIX] deletes fetchurl extension
* 5f7e5985 [TASK] allow to slice 9.x branch as well
* 830ead6f [TASK] Update newssync syncall command controller
* 66f01571 [TASK] show created rector files
* dedee375 [TASK] harden the rector run
* d56e7e12 [TASK] Updates
* c55082de [TASK] drk_help cgl
* eafc12ec [TASK] allow rector to fail
* 6039d4f8 [TASK] change typo3scan directory logic
* 5bbb9684 [TASK] let the slicer only slice direct sub directories
* 97b7bbfa [TASK] run rector for direct childs only
* de464c15 [TASK] keep rector files always and update config
* de8044cb [TASK] ignore platform reqs for rector script
* 63654205 [BUGFIX] ensure composer install was executed before for rector
* c50ae178 [TASK] add rector linting to the ci pipeline
* 754b222b [TASK] rector drkservice_app_buttons
* 5f59ba49 [TASK] rector drkservice_drkangebote
* 0f083982 [TASK] rector drkservice_drkintern
* 3f580426 [TASK] rector drkservice_felogin_bvw
* 015e2b1b [TASK] rector drkservice_iframe
* 96301c48 [TASK] rector for dti_drkcms_distribution
* 2620005c [TASK] update local packages pathes
* 221fd45c [TASK] rector for form_legacy
* c4a40f14 [BUGFIX] broken json of composer
* 09c49a18 [TASK] rector drkservice_distribution_angebote
* 83ea80ed [TASK] rector drkservice_distribution_intern
* 8f02da13 [TASK] rector drkservice_distribution_kv
* 9e5992fc [TASK] rector drkservice_distribution_lv
* 677cd067 [TASK] rector drkservice_distribution_ov
* dba859a3 [TASK] rector drkservice_distribution_ww
* 45949adc [TASK] rector newssync
* d5ad26b6 [TASK] rector lightroom
* 4ee9186f [TASK] adjust 4viewture Mailadresses
* 9bc131a6 [TASK] rector impexphelper
* 57353b2c [TASK] correct pathname for impexphelper
* 81881f52 [TASK] rector fal_webdav
* 47610f0f [TASK] Update phpstorm config for TYPO3 10.4
* 4b109c9a [TASK] add base for dti distribution
* 24717527 [TASK] Update editorconfig
* 03bc6000 Merge branch 'master' of gitlab.com:drk-intern/typo3-main-distribution
* 22f42110 [TASK] remove old rector file
* 9217ae62 [TASK] move drkservice_drkintern and fix broken .gitgnore file
* 2992727a [TASK] move drksservice_app_buttons
* 18e46d8d [TASK] move drkservice_drkangebote
* 695ec617 [TASK] move drkservice_distribution_angebote
* ddf2beb4 [TASK] move drkservice_distribution_intern
* dd62ab80 [TASK] move drkservice_distribution_kv
* 666dd4d1 [TASK] move drkservice_distribution_lv
* 4a4b2042 [TASK] move drkservice_distribution_ov
* d2c7439f [TASK] mode drkservice_distribution_ww
* c5d4d835 [TASK] move drk_addresses
* 9adb8848 [TASK] move drk_donate
* 516b1043 [TASK] move drk_clothescontainersearch
* 6176c8da [TASK] move drk_clothescontainerview
* a0727da2 [TASK] move drk_contactform
* cec2fd3b [TASK] move drk_courseregistration
* 58ef0d29 [TASK| move drk_coursesearch
* 85ec3a2c [TASK] move drkservice_courseview
* 94a8f537 [TASK] move drk_distribution2016
* feb3983a [TASK] move drk_general
* abdd92b8 [TASK] move drk_honoryform
* 98ef83dc [TASK] move drk_help
* 647ebc11 [TASK] followup zum drk_supplyfinder
* ce324334 [TASK] move drk_jobboard
* bd12df12 [TASK] move drk_memberform
* 15bf233a [TASK] move drk_orderform
* 9a6a0137 [TASK] move drk_supplyfinder
* 38df5eaa [TASK] move drk_template2016
* 24f1c077 [TASK] move drkservice_felogin_bvw
* 80a516a5 [TASK] move drkservice_iframe
* 49f10dfb [TASK] move form_legacy
* 8524fb06 [TASK] move frs_drk_calltoaction
* 4db8e21c [TASK] move frs_drk_linklist
* 85b06337 [TASK] move fal_webdav
* 01aa744a [TASK] move impexphelper
* f3fa7466 [TASK] move lightroom
* 30f9cc58 [TASK] move newssync
* 2f3c99fa [TASK] avoid PATH_site
* 3ede583c Update pages.yml
* 7454e5e4 [TASK] set typo3 scanner up to 10
* 33d1f277 [TASK] Update Viewhelper
* db44fff0 [TASK] remove form/legacy
* 3c64f03b [TASK] add extension builder remove pagespeed from —dev
* e980351a [TASK] install friendsoftypo3/form-legacy
* ea5644d3 [BUGFIX] newssync
* 251ff869 [TASK] require "ichhabrecht/filefill"
* 1cd6bd69 [TASK] require "haassie/page-speed-insights"
* b698afe0 [TASK] typos in extensionnames
* 2c9b8c3d [TASK] extbase bugfixing for @inject
* 99e1eca8 [TASK] raise gitlab ci php versions
* d1a1a0f6 [TASK] step 1 clear all dependencies
* 4029dc4b [TASK] Raise System requirements
* 47a00142 [DOC] Semver
* 9263ec28 [TASK] ignore Packagestates.php
* 60ae3f29 [TASK] allow to test local pages
* dff9b73d Merge branch 'master' of gitlab.com:drk-intern/typo3-main-distribution
* 5ac5149f [TASK] Update selenium firefox
* e1a8534e [TASK] Update Slicer
* 7ccae176 [TASK] add selenium base testing

