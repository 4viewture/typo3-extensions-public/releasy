<?php

namespace KayStrobach\Releasy\Traits;

use Symfony\Component\Console\Output\OutputInterface;

trait ChangelogTrait
{
    public function getChangelog(OutputInterface $output): string
    {
        $content = PHP_EOL . '* '
            . implode(
                PHP_EOL . '* ',
                $this->getCommandOutput(
                    'git log $(git describe --tags --abbrev=0)..HEAD --pretty=format:"%h %s"',
                    $output
                )
            );
        return $content;
    }
}
