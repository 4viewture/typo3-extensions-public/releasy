<?php

namespace KayStrobach\Releasy\Command\TYPO3;

use KayStrobach\Releasy\Command\AbstractCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;

class SetExtensionVersionCommand extends AbstractCommand
{
    protected static $defaultName = 'typo3:extension:version:set';

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Creates a new release and pushes it to the server')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command updates all included extension versions pushes the command to the server')
            ->addArgument('version', InputArgument::REQUIRED, 'version or tag name')
        ;
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    )
    {
        $version = $input->getArgument('version');

        $finder = new Finder();
        $finder->in('DistributionPackages')->name('ext_emconf.php');

        foreach($finder->getIterator() as $item)
        {
            $content = preg_replace(
                '#\'version\' => \'[^\']*\'#',
                '\'version\' => \'' . $version . '\'',
                $item->getContents()
            );
            file_put_contents($item->getPathname(), $content);
        }
        return 0;
    }
}
