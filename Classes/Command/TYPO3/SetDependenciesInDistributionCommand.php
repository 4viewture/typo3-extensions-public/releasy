<?php

namespace KayStrobach\Releasy\Command\TYPO3;

use KayStrobach\Releasy\Command\AbstractCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;

class SetDependenciesInDistributionCommand extends AbstractCommand
{
    protected static $defaultName = 'typo3:extension:distribution:dependencies';

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Set the dependencies in composer.json for extensions in distribution')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command updates all included extension versions pushes the command to the server')
            ->addArgument('version', InputArgument::REQUIRED, 'version or tag name')
        ;
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    )
    {
        $version = $input->getArgument('version');

        $finder = new Finder();
        $finder->in('DistributionPackages/Distributions')->name('composer.json');

        foreach($finder->getIterator() as $item)
        {
            $content = json_decode(file_get_contents($item->getPathname()), JSON_OBJECT_AS_ARRAY);

            foreach ($content['require'] as $key => $dependency) {
                if (strpos($key, 'drk') === 0) {
                    $content['require'][$key] = $version;
                }
            }
            file_put_contents(
                $item->getPathname(),
                json_encode(
                    $content,
                    JSON_PRETTY_PRINT
                ) . PHP_EOL
            );
        }
        return 0;
    }
}
