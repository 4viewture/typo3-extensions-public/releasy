<?php

namespace KayStrobach\Releasy\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Finder\Finder;
use z4kn4fein\SemVer\Version;

class ReleaseCommand extends AbstractCommand
{
    protected static $defaultName = 'releasy:release';

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Creates a new release and pushes it to the server')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command updates all included extension versions pushes the command to the server')
            ->addArgument('version', InputArgument::REQUIRED, 'version or tag name')
        ;
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    )
    {
        $this->execInternalCommand('git:check', $input, $output);

        // do minor or patch release raise of version here

        $version = $input->getArgument('version');
        $output->writeln('Preparing to release ' . $version);

        $this->execInternalCommand('typo3:extension:version:set', $input, $output);

        #$content = $this->requestText(
        #    'Would you like to add a release description?',
        #    $input,
        #    $output
        #);

        $this->execInternalCommand('typo3:extension:distribution:dependencies', $input, $output);
        $this->execInternalCommand('git:dumpchangelog', $input, $output);



        if (!$this->confirm('Commit and release the new version?', $input, $output)) {
            return 1;
        }

        $this->execInternalCommand(
            'git:tag:create',
            $input,
            $output
        );



        return 0;
    }

    protected function getDevVersion($version)
    {
        $version = Version::parse($version);
        $newVersion = Version::parse($version->getMajorVersion() . '');
    }
}
