<?php
/**
 * Created by kay.
 */

namespace KayStrobach\Releasy\Command\Git;

use KayStrobach\Releasy\Command\AbstractCommand;
use KayStrobach\Releasy\Traits\ChangelogTrait;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GitCreateTagCommand extends AbstractCommand
{
    protected static $defaultName = 'git:tag:create';

    use ChangelogTrait;

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('creates a git tag')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('just create the tag with the needed information')
            ->addArgument('version', InputArgument::REQUIRED, 'version or tag name')
        ;
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    )
    {
        $version = $input->getArgument('version');

        $this->execCommand('git add *', $output);

        $content = '[RELEASE] ' . $version . PHP_EOL . PHP_EOL . $this->getChangelog($output);

        $this->execCommand('git commit -am ' . escapeshellarg($content), $output);
        $this->execCommand('git tag -a ' . $version . ' -m ' . escapeshellarg($content), $output);

        return 0;
    }
}
