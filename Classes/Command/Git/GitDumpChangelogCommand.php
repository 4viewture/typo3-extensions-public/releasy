<?php

namespace KayStrobach\Releasy\Command\Git;

use KayStrobach\Releasy\Command\AbstractCommand;
use KayStrobach\Releasy\Traits\ChangelogTrait;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GitDumpChangelogCommand extends AbstractCommand
{
    protected static $defaultName = 'git:dumpchangelog';

    use ChangelogTrait;

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Updates the Changelog.md')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('Ensures, that the gitlog ist stored correctly')
            ->addArgument('version', InputArgument::REQUIRED, 'version or tag name')
        ;
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    )
    {
        $version = $input->getArgument('version');
        $content = $this->getChangelog($output);

        $changelogPath = 'Changelog.md';

        file_put_contents(
            $changelogPath,
            '# Version ' . $version . PHP_EOL
            . $content . PHP_EOL
            . PHP_EOL
            . file_get_contents($changelogPath)
        );
        return 0;
    }
}
