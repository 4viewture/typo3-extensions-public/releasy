<?php

namespace KayStrobach\Releasy\Command\Git;

use KayStrobach\Releasy\Command\AbstractCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CheckCommand extends AbstractCommand
{
    protected static $defaultName = 'git:check';

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Checks for uncomitted changes')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('Ensures, nothing is uncommitted')
        ;
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    )
    {
        if (!$this->isCommandOk('git diff --exit-code')) {
            $output->writeln('<error>Please ensure you have committed all your changes! Use git stash to store uncomitted changes</error>');
            return 10;
        }
        return 0;
    }
}
